var gulp = require('gulp'),
    concat = require('gulp-concat'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    cssmin = require('gulp-cssmin'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    minify = require('gulp-babel-minify'),
    babel = require('gulp-babel'),
    webpack = require('gulp-webpack');

var historyApiFallback = require('connect-history-api-fallback');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;




gulp.task('styles', function() {
    gulp.src('src/scss/*.scss')
        .pipe(sass())
        .pipe(prefixer({
            browsers: ['last 15 versions'],
            cascade: false
        }))
        .pipe(concat('styles.css'))
        .pipe(cssmin())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('app/css/'))
        .pipe(reload({
            stream: true
        }));
});


gulp.task('scripts', function () {
    gulp.src('src/js/*.js')
        .pipe(babel())
        .pipe(concat('main.js'))
        .pipe(minify())
        .pipe(rename({
          suffix: '.min'
      }))
        .pipe(gulp.dest('app/js/'))
});


gulp.task('serve', ['scripts', 'styles'], function() {
    browserSync.init({
        server: {
            baseDir: "./",
            middleware: [historyApiFallback({})]
        }
    });

    gulp.watch('src/scss/*.scss', ['styles']).on('change', browserSync.reload);
    gulp.watch('src/js/*.js', ['scripts']).on('change', browserSync.reload);
});
