'use strict';

let HttpClient = function() {
  this.get = function(aUrl, aCallback) {
    let anHttpRequest = new XMLHttpRequest();
    anHttpRequest.onreadystatechange = function() {
      if (anHttpRequest.readyState === 4 && anHttpRequest.status === 200)
        aCallback(anHttpRequest.responseText);
    };

    anHttpRequest.open('GET', aUrl, true);
    anHttpRequest.send(null);
  };
};
const client = new HttpClient();
const searchSubmitBtn = document.getElementById('search__submit-btn');
const searchInput = document.getElementById('searchInput');
const searchResult = document.getElementById('search-result');
const paginateContainer = document.getElementById('paginate-container');

searchInput.onkeyup = () => {
  if (searchInput.value) {
    searchSubmitBtn.removeAttribute('disabled');
  } else {
    searchSubmitBtn.setAttribute('disabled', true);
  }
};


let repositories = [];
let responseObj = {};
let maxResponseResults = 1000;
let totalResultCount;
let currentPage = 1;
let per_page = 12;

let numberOfPages;

let setResponseMaxCount = () => {
  if (responseObj.total_count < maxResponseResults) {
    totalResultCount = responseObj.total_count;
  } else {
    totalResultCount = maxResponseResults;
  }

  return (numberOfPages = Math.ceil(totalResultCount / per_page));
};

let search = pageNum => {
  let lastInputValue;

  if (searchInput.value) lastInputValue = searchInput.value;

  client.get(
    `https://api.github.com/search/repositories?q=${searchInput.value ||
      lastInputValue ||
      'javascript'}&page=${pageNum}&per_page=12`,
    function(response) {
      repositories = JSON.parse(response).items;
      responseObj = JSON.parse(response);
      fillReposContainer();
      
      togglePagination();
    }
  );
};

let clickHandler = event => {
  event.preventDefault();

  if (searchInput.value) {
    searchResult.innerHTML = '';
    search(currentPage);
  }
};

function deleteItem(event, repoIndex) {
  let index = repoIndex;

  if (index > -1) {
    document.getElementById(`repository${index}`).remove();
    repositories.splice(index, 1);
  }
  togglePagination();
}

function fillReposContainer() {
  for (let repo of repositories) {

    let data = repo;

    let reposContainer = document.createElement('li');
    reposContainer.className = 'repository';
    reposContainer.setAttribute(
      'id',
      `repository${repositories.indexOf(repo)}`
    );


    // repository card element
    let repositoryCard = document.createElement('div');
    repositoryCard.setAttribute('id', 'gh-app-repository');

    

    let repoUpdateDate = new Date(Date.parse(data.updated_at));

    let repoLanguage = () => {
      let repoLangCheck;
      if (data.language) {
        return (repoLangCheck = /*html*/ `<span class="repository-code-lang">Language: ${data.language}</span>`);
      } else {
        return '';
      }
    };

    repositoryCard.innerHTML = /*html*/`
    <div class="repository-top">
    <div class="repository__col-9 repository__col-left">
        <a class="repository-link" href="${data.html_url}">${
      data.owner.login
    } / ${data.name}</a>
        <span class="repository-stats__stars">
          <svg aria-label="star" class="octicon octicon-star" height="16" role="img" version="1.1" viewBox="0 0 14 16" width="14">
            <path fill-rule="evenodd" d="M14 6l-4.9-.64L7 1 4.9 5.36 0 6l3.6 3.26L2.67 14 7 11.67 11.33 14l-.93-4.74z"></path>
          </svg>
          ${data.stargazers_count}
        </span>

        <span class="repository-owner">Owner: <a href='${
          data.owner.html_url
        }'>${data.owner.login}</a></span>
        <p class="repository-description">${data.description}</p>
    </div>

    <div class="repository__col-3 repository__col-right">
        <img class="repository-pic" src='${data.owner.avatar_url}' />
    </div>
</div>

<div class="repository-bottom">
    <div class="repository__col-6 repository__col-left">
        <div class="repository-stats">
            <span class="repository-stats__issues">issues: <strong>${
              data.open_issues_count
            }</strong></span>
        </div>
        <span class="repository-last-update">Updated: ${repoUpdateDate}</span>
    </div>

    <div class="repository__col-6 repository__col-right">
        ${repoLanguage()}
    </div>
</div>
<button class="repository-delete-btn" onClick="deleteItem(event, ${repositories.indexOf(
      data
    )})"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 469.785 469.785" width="12px" height="12px"><path d="M285.362 234.691L459.34 60.715c13.893-13.88 13.893-36.391 0-50.27-13.88-13.894-36.377-13.894-50.27 0L235.09 184.42 61.1 10.444c-13.865-13.894-36.391-13.894-50.27 0-13.88 13.88-13.88 36.391 0 50.27L184.82 234.69 10.445 409.068c-13.894 13.893-13.894 36.39 0 50.27 6.94 6.954 16.04 10.424 25.142 10.424 9.087 0 18.189-3.47 25.13-10.424L235.09 284.962 409.07 458.94c6.954 6.94 16.04 10.424 25.142 10.424a35.458 35.458 0 0 0 25.13-10.424c13.893-13.88 13.893-36.376 0-50.27L285.361 234.691z" data-original="#DD2E44" class="active-path" fill="#DD2E44"/></svg></button>
`;
    searchResult.appendChild(reposContainer);
    reposContainer.appendChild(repositoryCard);
  }
  togglePagination();
}



let showPrevLinkCheck = () => {
  let prevLink;
   if(currentPage > 1) { return (prevLink = /*html*/`<a class="previous_page" rel="prev start" href="javascript:prevPage()"><< previous</a> <span class="gap">…</span>`);
  } else {
    return '';
  }
};

let nextPage = () => {
  ++currentPage;
  paginateContainer.innerHTML = '';
  searchResult.innerHTML = '';
  repositories = [];
  search(currentPage);
  
};

let prevPage = () => {
  --currentPage;
  paginateContainer.innerHTML = '';
  searchResult.innerHTML = '';
  repositories = [];
  search(currentPage);
};



let togglePagination = () => {
  
  if (repositories.length) paginateContainer.innerHTML = /*html*/`
  <div class="pagination" >
   ${showPrevLinkCheck()}
       <a class="next_page" rel="next" href="javascript:nextPage()">next >></a></div>
`;
  else paginateContainer.innerHTML = '';
};
